# 👋 Welcome to my profile

My name is Unai Medina and I am a Junior Developer. Here, you can find information about my work and my skills.

## 🙍 ¬ About Me

I am deeply passionate about programming and technology, finding joy in continually learning about new programming languages and exploring various domains of software development.

I have studied: 
- Microcomputer Systems and Networks
- Network Systems Administration with a cybersecurity profile (ASIX, first year)
- Multiplatform Application Development (DAM), currently enrolled in the second year.

Technical Skills

I have basic knowledge in:
- Programming languages: **Java, C++, JavaScript, HTML, CSS.**
- Databases: **Oracle DB, MySQL, MongoDB.**

## 📕 ¬ My Projects

I have initiated several projects, both personal and collaborative, aimed at improving user experience and endeavoring to launch new products or services. Below, you will find a record of some of these projects on the platform:

### ⚡ Voltic Studios

Voltic Studios is a collaborative group of colleagues committed to enhancing our code and providing solutions tailored to small businesses.
- Github: [Voltic Studios](https://github.com/Voltic-Studios)
- X/Twitter: [@VolticStudio](https://twitter.com/VolticStudio)
- Website: [www.voltic.site](https://voltic.site)

## 🤝 ¬ Contact

I'd be thrilled to connect with you! You can reach me on the following platforms:

- GitHub: [umedina](https://github.com/unaimedina)
- LinkedIn: [Unai Medina Fernández](https://www.linkedin.com/in/unai-medina-fdez/)
- Email: [umdafdez@gmail.com](mailto:umdafdez@gmail.com)
- Website: [unai.space](https://unai.space)

Thank you for visiting my GitHub profile! Feel free to reach out if you have any questions or if you're interested in collaborating on a project. Don't hesitate to contact me.
